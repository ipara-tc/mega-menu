$( document ).ready(function() {
  // Handler for .ready() called.
  // /* Prevent menu close, outside click */
  $('#tcMegaMenu').on('hide.bs.dropdown', function (e) {
    if (e.clickEvent) {
      e.preventDefault();
    }
  });

  /* Rotate the caret onClick, add an active class to menu items */
  $('.mega-menu .navbar-nav .nav-item .nav-link').click(function() {
    $(this).toggleClass('nav-item--active');
    $(this).children('.nav-icon').toggleClass('nav-icon--rotate');
    $(this).closest('.navbar-nav').find('.nav-item .nav-link').not(this).removeClass('nav-item--active');
    $(this).closest('.navbar-nav').find('.nav-item .nav-link').not(this).children('.nav-icon').removeClass('nav-icon--rotate');
  });

  /* Rotate the caret onClick, add an active class to submenu items */
  $('.mega-menu .navbar-nav .dropdown-submenu-item').click(function() {
    /* $(this).toggleClass('nav-item--active'); */
    $(this).children('.submenu-nav-icon').toggleClass('nav-icon--rotate');
    /* $(this).closest('.navbar-nav').find('.dropdown-submenu-item').not(this).removeClass('nav-item--active'); */
    $(this).closest('.navbar-nav').find('.dropdown-submenu-item').not(this).children('.submenu-nav-icon').removeClass('nav-icon--rotate');
  });

  /* Hamburger Icon */
  $(function() {
    $('#navbarSupportedContent')
      .on('shown.bs.collapse', function() {
        $('#toggler-hamburger').addClass('display-none');
        $('#toggler-close').removeClass('display-none');
      })
      .on('hidden.bs.collapse', function() {
        $('#toggler-hamburger').removeClass('display-none');
        $('#toggler-close').addClass('display-none');
      });
  });
});

/* Mega menu on hover display submenu */
/*
const $dropdown = $(".dropdown");
const $dropdownToggle = $(".dropdown-toggle");
const $dropdownMenu = $(".dropdown-menu");
const showClass = "show";

$(window).on("load resize", function() {
  if (this.matchMedia("(min-width: 768px)").matches) {
    $dropdown.hover(
      function() {
        const $this = $(this);
        $this.addClass(showClass);
        $this.find($dropdownToggle).attr("aria-expanded", "true");
        $this.find($dropdownMenu).addClass(showClass);
      },
      function() {
        const $this = $(this);
        $this.removeClass(showClass);
        $this.find($dropdownToggle).attr("aria-expanded", "false");
        $this.find($dropdownMenu).removeClass(showClass);
      }
    );
  } else {
    $dropdown.off("mouseenter mouseleave");
  }
});
*/

/*
const targetDiv = document.getElementById("third");
const btn = document.getElementById("toggle");
btn.onclick = function () {
  if (targetDiv.style.display !== "none") {
    targetDiv.style.display = "none";
  } else {
    targetDiv.style.display = "block";
  }
};
*/
/*
$(document).click(function() {
  if($('.mega-menu .navbar-nav li a .nav-icon').hasClass('nav-icon--rotate')) {
    $('.navbar-nav li a .nav-icon').removeClass('nav-icon--rotate');
  }
})
/*
$("body").on("click", function(event) {
  event.stopPropagation();
  $(".mega-menu .navbar-nav li a").toggleClass("nav-icon--rotate");
});
*/
/*
$('.navbar-nav li a').click(function() {
    if($(this).children('.nav-icon').hasClass('nav-icon--rotate')) {
        $('.navbar-nav li a .nav-icon').removeClass('nav-icon--rotate');
    } else {
        $('.navbar-nav li a .nav-icon').removeClass('nav-icon--rotate');
        $(this).children('.nav-icon').addClass("nav-icon--rotate");
    }
});
*/
