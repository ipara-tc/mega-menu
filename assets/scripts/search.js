/* Search feature */
function searchToggle(obj, evt) {
  var container = $(obj).closest('.search-wrapper');
  var navbar = document.getElementById('tcMegaMenuNavbar');
  var contactBtn = document.getElementById('contactMainBtn');
    if (!container.hasClass('active')) {
      container.addClass('active');
      navbar.style.display = 'none';
      contactBtn.style.display = 'none';
      evt.preventDefault();
    } else if (container.hasClass('active') && $(obj).closest('.input-holder').length == 0) {
      container.removeClass('active');
      navbar.style.display = 'flex';
      contactBtn.style.display = 'block';
      // clear input
      container.find('.search-input').val('');
  }
}
